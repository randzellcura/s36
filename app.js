// Importing the dependencies/modules
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoute = require('./routes/taskRoutes')

dotenv.config()

const app = express()
const port = 3003
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

mongoose.connect(`mongodb+srv://ivanc:${process.env.MONGODB_PASSWORD}@cluster0.asallxv.mongodb.net/S36-Activity?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
let db = mongoose.connection

db.on('error', () => console.error('Connection error.'))
db.on('open', () => console.log('We are connected to MongoDB!'))

// Routes
app.use('/tasks', taskRoute)

// Server listening
app.listen(port, () => console.log(`Server running on localhost:${port}`))